__author__ = 'pphaneuf'


import unittest
import collections


from AutoBreseq import parse_sample_name
from AutoBreseq import get_local_sample_file_list
from AutoBreseq import remove_sample_duplicates
from AutoBreseq import get_local_samples
from AutoBreseq import is_sample_file
from AutoBreseq import get_sample_file_dict
from AutoBreseq import get_reference_name
from AutoBreseq import get_breseq_cmd


class TestAutoBreseq(unittest.TestCase):

    def setUp(self):

        self.sample_file_name = "Crooks-10-8-1_S21_L001_R1_001.fastq"

        self.reference_file = "NC_010468_Crooks_cfb.gbk"

        self.sample_file_list = ['not_a_sample_file',
                                 'Crooks-23-1_S24_L001_R1_001.fastq',
                                 'Crooks-10-8-2_S22_L001_R2_001.fastq']

        self.duplicate_sample_file_list = ['asdf',
                                           'asdf',
                                           'zxcv']

        self.sample_file_list_for_dict = ['asdf-1_S24_L001_R1_001.fastq',
                                          'asdf-1_S24_L001_R2_001.fastq',
                                          'asdf-10_S22_L001_R1_001.fastq',
                                          'asdf-10_S22_L001_R2_001.fastq']


# TODO: fix this unit test
#    def test_get_breseq_cmd(self):
#
#        expected = ['breseq', '-p', '-j 8', '--polymorphism-frequency-cutoff 0.05', '-r U00096.3.gb', '-o 10Xyl_S12_L001', '10Xyl_S12_L001_R1_001.fastq', '10Xyl_S12_L001_R1_001_I-Rabbers.fastq', '10Xyl_S12_L001_R2_001.fastq']
#
#        sample_name = "10Xyl_S12_L001"
#
#        sample_read_files = ["10Xyl_S12_L001_R1_001.fastq",
#                             "10Xyl_S12_L001_R1_001_I-Rabbers.fastq",
#                             "10Xyl_S12_L001_R2_001.fastq"]
#
#        output = get_breseq_cmd(sample_name, sample_read_files)
#
#        self.assertEqual(output, expected)

    def test_reference_file_name(self):

        expected = "NC_010468_Crooks_cfb"

        output = get_reference_name(self.reference_file)

        self.assertEqual(output, expected)

    def test_get_sample_file_dict(self):

        expected = collections.defaultdict(list)
        expected["asdf-1"].append("asdf-1_S24_L001_R1_001.fastq")
        expected["asdf-1"].append("asdf-1_S24_L001_R2_001.fastq")
        expected["asdf-10"].append("asdf-10_S22_L001_R1_001.fastq")
        expected["asdf-10"].append("asdf-10_S22_L001_R2_001.fastq")

        output = get_sample_file_dict(self.sample_file_list_for_dict)

        self.assertEquals(expected, output)

    def test_get_local_samples(self):

        sample_file_list = ['Crooks-23-1_S24_L001_R1_001_1.fastq',
                            'Crooks-23-1_S24_L001_R2_001_1.fastq',
                            'Crooks-23-1_S24_L001_R1_001_2.fastq',
                            'Crooks-23-1_S24_L001_R2_001_2.fastq',
                            'Crooks-10-8-2_S22_L001_R1_001_1.fastq',
                            'Crooks-10-8-2_S22_L001_R2_001_1.fastq',
                            'Crooks-10-8-2_S22_L001_R1_001_2.fastq',
                            'Crooks-10-8-2_S22_L001_R2_001_2.fastq']

        expected = ["Crooks-23-1", "Crooks-10-8-2"]

        output = get_local_samples(sample_file_list)

        self.assertEquals(set(expected), set(output))

    def test_parse_sample_name(self):

        expected = "Crooks-10-8-1"

        output = parse_sample_name(self.sample_file_name)

        self.assertEquals(expected, output)

    def test_get_local_sample_file_list(self):

        expected = ['Crooks-23-1_S24_L001_R1_001.fastq', 'Crooks-10-8-2_S22_L001_R2_001.fastq']

        output = get_local_sample_file_list(self.sample_file_list)

        self.assertEquals(expected, output)

    def test_remove_sample_file_duplicates(self):

        expected = ['asdf', 'zxcv']

        output = remove_sample_duplicates(self.duplicate_sample_file_list)

        self.assertEquals(set(expected), set(output))

    def test_is_sample_file(self):

        output = is_sample_file(self.sample_file_list[0])
        self.assertFalse(output)

        output = is_sample_file(self.sample_file_list[1])
        self.assertTrue(output)
