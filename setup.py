#!/usr/bin/env python

from distutils.core import setup

setup(
    name='AutoBreseq',
    version='0.1',
    description='To execute Breseq on many FASTQs against the same reference',
    py_modules=['AutoBreseq'],
    author='Patrick Phaneuf',
    author_email='pphaneuf@eng.ucsd.edu',
)
